/**
 * Created by Vitalii on 14.05.2022.
 */

public  class primaryAccountContact {

     public static void  acUpdate(List<AccountContact__c> Tnew, List<AccountContact__c> Told ){
         for(AccountContact__c ACn: Tnew){
             if(ACn.isPrimary__c == true){
                 if(Told[0].isPrimary__c == false){
                     List<AccountContact__c> updateList = [SELECT Account__c, isPrimary__c  FROM AccountContact__c WHERE Account__c = : ACn.Account__c AND isPrimary__c = : true];
                     for(AccountContact__c  AccCon :updateList  ){
                         AccCon.isPrimary__c =false;
                     }
                     recursionCheck.firstRun = false;
                     update updateList;
                 }
             } else{
                 if(Told[0].isPrimary__c == true){
                     AccountContact__c updateAccCon = [SELECT CreatedDate, Account__c  FROM AccountContact__c WHERE Account__c = : ACn.Account__c AND CreatedDate <> :ACn.CreatedDate   ORDER BY CreatedDate DESC  Limit 1];
                     if(updateAccCon != null ){
                         System.Debug(updateAccCon);
                         updateAccCon.isPrimary__c = true;
                         recursionCheck.firstRun = false;
                         update updateAccCon;
                     }
                 }
             }
         }
     }

    public static void  acInsert(List<AccountContact__c> Tnew){
        for(AccountContact__c AC: Tnew ){
            if(AC.isPrimary__c){
                List<AccountContact__c> acCheck = [SELECT Account__c, Name, isPrimary__c FROM AccountContact__c WHERE   Account__c = : AC.Account__c AND isPrimary__c = true AND Name <> :AC.Name  Limit 1];
                if(!acCheck.isEmpty()){
                    acCheck[0].isPrimary__c = false;
                }
                recursionCheck.firstRun = false;
                update acCheck;
                System.Debug('Primary');
            }else{
                List<AccountContact__c> acCheck = [SELECT Account__c, Contact__c, isPrimary__c FROM AccountContact__c WHERE   Account__c = : AC.Account__c AND isPrimary__c= true Limit 1];
                if(acCheck.isEmpty()){
                    AC.isPrimary__c = true;
                    recursionCheck.firstRun = false;
                }
                System.Debug('noPrimary');
            }
        }
    }
    public static void  acDelete(List<AccountContact__c> Told){
        for(AccountContact__c AC: Told ){
            if(AC.isPrimary__c){
                AccountContact__c updateAccCon = [SELECT CreatedDate, Account__c  FROM AccountContact__c WHERE Account__c = : AC.Account__c AND CreatedDate <> :AC.CreatedDate   ORDER BY CreatedDate DESC  Limit 1];
                if(updateAccCon != null ){
                    System.Debug(updateAccCon);
                    updateAccCon.isPrimary__c = true;
                    recursionCheck.firstRun = false;
                    update updateAccCon;
                }
            }
    }

}
}