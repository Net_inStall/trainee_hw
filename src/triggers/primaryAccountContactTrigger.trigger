/**
 * Created by Vitalii on 14.05.2022.
 */
//update
trigger primaryAccountContactTrigger on AccountContact__c (before insert, before update, after delete) {
  if(recursionCheck.firstRun) {
      if (Trigger.isinsert) {
          primaryAccountContact.acInsert(Trigger.new);
      } else if (Trigger.isupdate) {
          primaryAccountContact.acUpdate(Trigger.new, Trigger.old);
      } else if (Trigger.isdelete) {
          primaryAccountContact.acDelete(Trigger.old);
      }
      }
  }
  }


